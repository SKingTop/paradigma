$(document).ready(function(){
    $('[header-open]').on('click', function(){
        $(this).toggleClass('active');
        $('header').toggleClass('open');
        $('.page').toggleClass('no-scroll');
    });
});
