$(document).ready(function () {
    $('.phoneMask_JS').mask('+7 ( 0 0 0 ) 0 0 - 0 0 - 0 0');

    $('[agreement-controll-checkbox]').on('click', function () {
        $element = $(this);
        $checkbox = $element.find('[type="checkbox"]');
        $targerBtn = $element.closest('form').find('[agreement-controll-btn]');

        if ($(this).hasClass('checked')) {
            $element.removeClass('checked');
            $checkbox.find('[type="checkbox"]').removeAttr('checked');
            $targerBtn.addClass('disabled');
        } else {
            $element.addClass('checked');
            $checkbox.find('[type="checkbox"]').attr('checked', 'checked');
            $targerBtn.removeClass('disabled');
        }
    });

    $('[popup-open]').on('click', function () {
        $element = $(this);
        $targetPopup = $element.attr('popup-open');

        popupClose();

        $('[popup-name="' + $targetPopup + '"').addClass('open');
        $('.page').addClass('no-scroll');

    });

    $('[popup-close]').on('click', function () {
        popupClose();
    });

    $("form").submit(function (e) {
        e.preventDefault();
    });
});

var popupClose = function () {
    $('[header-open]').removeClass('active');
    $('header').removeClass('open');
    $('.popup.open').find('[type="checkbox"]').removeAttr('checked');
    $('.popup.open form').find("input[type=text], textarea").val("");
    $('.popup.open form').find('[agreement-controll-btn]').addClass('disabled');
    $('.popup.open [agreement-controll-checkbox]').removeClass('checked');
    $('.popup.open').removeClass('open');
    $('.page').removeClass('no-scroll');
}
